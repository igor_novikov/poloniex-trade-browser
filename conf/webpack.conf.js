const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');

module.exports = {
  mode: 'development',
  module: {
		rules: [
			{
				test: /\.json$/,
				use: 'json-loader',
			},
			{
				test: /\.(css|scss)$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader',
					'postcss-loader',
				],
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: [
					'babel-loader',
				]
			}
		]
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		FailPlugin,
		new HtmlWebpackPlugin({
			template: conf.path.src('index.html')
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.LoaderOptionsPlugin({
			debug: true
		})
	],
	devtool: 'source-map',
	output: {
		path: path.join(process.cwd(), conf.paths.tmp),
		filename: 'index.js'
	},
	entry: [
		'webpack/hot/dev-server',
		'webpack-hot-middleware/client',
		`./${conf.path.src('index')}`
	],
};
