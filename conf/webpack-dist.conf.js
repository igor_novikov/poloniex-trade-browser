const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const pkg = require('../package.json');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  mode: 'production',
	module: {
		rules: [
			{
				test: /\.(css|scss)$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader?minimize!sass-loader!postcss-loader'
				})
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: 'babel-loader',
			}
		]
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		FailPlugin,
		new HtmlWebpackPlugin({
			template: conf.path.src('index.html')
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': '"production"'
		}),
		new ExtractTextPlugin('index-[hash].css'),
	],
  optimization: {
    minimizer: [
      new UglifyJsPlugin ({ uglifyOptions: {
        output: {comments: false},
        compress: {unused: true, dead_code: true, warnings: false} // eslint-disable-line camelcase
      }})
    ],
  },
	output: {
		path: path.join(process.cwd(), conf.paths.dist),
		filename: '[name]-[hash].js'
	},
	entry: {
		app: `./${conf.path.src('index')}`,
		vendor: Object.keys(pkg.dependencies)
	}
};
