# [Poloniex Trade Browser](http://poloniex-trade-browser.igor-novikov.me/)

This is a small sample project to showcase my skillset, in particular with React, Material-UI, React Router, Redux and Redux-Saga. This was made in about two days.

The project is a very simple frontend to show latest trades on the [Poloniex](https://poloniex.com/) crypto asset exchange. User can choose two currencies to view latest trades performed on the exchange between these currencies. List of currencies, available currency pairs and trades are loaded via Poloniex public API.

You can check it out live at http://poloniex-trade-browser.igor-novikov.me/
