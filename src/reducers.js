import Immutable from 'seamless-immutable'
import mapValues from 'lodash/mapValues'
import pickBy from 'lodash/pickBy'

const initialState = Immutable ({
	currencies: null,
	tradeHistoryByPair: {},
	primaryCurrencies: {},
	validPairs: {},
})

const rootReducer = (state, action) => {
	if (typeof state === 'undefined') return initialState
	switch (action.type) {
		case 'UPDATE_CURRENCIES':
			return state.set ('currencies', mapValues (
				pickBy (action.currencies, (currency, shortName) => (
					state.validPairs [shortName]
				)),
				(currency, shortName) => ({
					isPrimary: state.primaryCurrencies [shortName],
					shortName,
					...currency
				})
			))
		break

		case 'UPDATE_VALID_PAIRS':
			const validPairs = {}, primaryCurrencies = {}
			for (let pair of action.validPairs) {
				const [ left, right ] = pair.match (/[^_]+/g)
				primaryCurrencies [left] = true
				if (!validPairs [left]) validPairs [left] = {}
				if (!validPairs [right]) validPairs [right] = {}
				validPairs [left] [right] = validPairs [right] [left] = pair
			}
			return state.merge ({ validPairs, primaryCurrencies })
		break

		case 'UPDATE_TRADE_HISTORY':
			return state.setIn (['tradeHistoryByPair', action.currencyPair], action.tradeHistory)
		break
	}
	return state
}

export default rootReducer
