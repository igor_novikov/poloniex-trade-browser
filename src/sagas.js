import { take, select, put, call } from 'redux-saga/effects'
import DateTime from 'luxon/src/datetime'
import pickBy from 'lodash/pickBy'

import { handleError, encodeQueryString, tradeCurrencyPair } from './util'

const debugEnabled = true

async function api (command, options = {}) {
	const request = await fetch ('https://poloniex.com/public?'+encodeQueryString ({ command, ...options }))
	return request.json()
}

function* getCurrencies() {
	const rawCurrencies = yield call (api, 'returnCurrencies')
	const currencies = pickBy (rawCurrencies, ({ disabled, delisted }) => true || !disabled && !delisted)
	yield put ({ type: 'UPDATE_CURRENCIES', currencies })
}

function* getValidPairs() {
	const volume = yield call (api, 'return24hVolume')
	const validPairs = Object.keys (pickBy (volume, pair => typeof pair === 'object'))
	yield put ({ type: 'UPDATE_VALID_PAIRS', validPairs })
}

function* getTradeHistory ({ leftCurrency, rightCurrency }) {
	const
		currencyPair = tradeCurrencyPair (leftCurrency, rightCurrency),
		now = DateTime.local(),
		start = now.minus ({ days: 1 }).toMillis() / 1000,
		end = now.toMillis() / 1000

	let isReverse = false
	let transactions = yield call (api, 'returnTradeHistory', {
		currencyPair, start, end
	})

	if (transactions.error) {
		const reversePair = currencyPair.replace (/^(.*)_(.*)$/, '$2_$1')
		transactions = yield call (api, 'returnTradeHistory', {
			currencyPair: reversePair,
			start, end,
		})

		if (transactions.error) {
			transactions = null
		} else {
			isReverse = true
		}
	}

	yield put ({
		type: 'UPDATE_TRADE_HISTORY',
		currencyPair,
		tradeHistory: {
			isAvailable: !!transactions,
			isReverse, transactions,
		}
	})
}

let confirmReady

function* rootSaga() {
	const actionToHandler = {
		GET_CURRENCIES: getCurrencies,
		GET_VALID_PAIRS: getValidPairs,
		GET_TRADE_HISTORY: getTradeHistory,
	}
	const availableActions = Object.keys (actionToHandler)

	if (debugEnabled) {
		console.log ('Available actions: '+availableActions.join (', '))
	}

	confirmReady()

	yield call (getValidPairs)
	yield call (getCurrencies)

	while (true) {
		const action = yield take (availableActions)

		if (debugEnabled) {
			console.log ('Saga', action)
		}

		try {
			yield call (actionToHandler [action.type], action)
		} catch (error) {
			handleError (error)
		}
	}
}

rootSaga.ready = new P (resolve => confirmReady = resolve)

export default rootSaga
