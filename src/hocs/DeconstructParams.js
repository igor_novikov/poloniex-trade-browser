import React from 'react'

const DeconstructParams = (Component) => ({ match: { params }, ...rest}) => (
	<Component {...rest} {...params} />
)

export default DeconstructParams
