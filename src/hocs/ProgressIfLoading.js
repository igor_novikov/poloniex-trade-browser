import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import { withStyles } from '@material-ui/core/styles'

const styles = {
	container: {
		display: 'flex',
		position: 'fixed',
		top: '0',
		right: '0',
		bottom: '0',
		left: '0',
		justifyContent: 'center',
		alignItems: 'center',
		pointerEvents: 'none',
	},
}

const ProgressIfLoading = (Component) => withStyles (styles) (
	class ProgressIfLoading extends React.PureComponent {
		componentDidMount() {
			this.componentDidUpdate()
		}

		componentDidUpdate() {
			const { isLoading, onMissingContent } = this.props
			isLoading && onMissingContent && onMissingContent()
		}

		render() {
			const { isLoading, classes, ...rest } = this.props
			return (
				isLoading ? (
					<div className={ classes.container }>
						<CircularProgress />
					</div>
				) :
				<Component {...rest} />
			)
		}
	}
)

export default ProgressIfLoading
