import { mapStackTrace } from 'sourcemapped-stacktrace'
import partial from 'lodash/partial'

export const d = window.d = (label, value) => {
	console.log (label, value)
	return value
}

export const encodeQueryString = object => {
	const encodedPairs = []
	for (let name in object) {
		encodedPairs.push (encodeURIComponent (name)+'='+encodeURIComponent (object [name]))
	}
	return encodedPairs.join ('&')
}

export const tradeCurrencyPair = (leftCurrency, rightCurrency) => (
	[leftCurrency, rightCurrency].join ('_')
)

export const handleError = error => {
	if (!error) return;
	if (!error.stack) {
		setImmediate (() => {
			throw new Error ('An uncaught plain error has been thrown: '+JSON.stringify (error, undefined, '    '))
		})
	} else {
		mapStackTrace (error.stack, stack => {
			error.stack = typeof error.stack === 'string' ? stack.join ('\n') : stack
			console.error (error, stack.join ('\n'))
		})
	}
}

window.onunhandledrejection = handleError

window.P = Promise

P.delay = (timeout = 0) => new P (resolve => setTimeout (resolve, timeout))

P.prototype.finally = function (callback) {
	let hasRunCallback = false
	const handler = (isError, value) => {
		hasRunCallback = true
		try {
			callback()
		} catch (e) {
			handleError (e)
		}
		if (isError) throw value
		else return value
	}
	return this.then (
		partial (handler, false),
		partial (handler, true)
	)
}
