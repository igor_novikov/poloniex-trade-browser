import React from 'react'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core/styles'

import ProgressIfLoading from '../hocs/ProgressIfLoading'
import CurrencyCardContainer from '../containers/CurrencyCardContainer'
import CurrencyCardList from './CurrencyCardList'

const styles = {
	searchField: {
		width: '100%',
	},
	searchFieldText: {
		fontSize: '2em',
	},
	gridContainer: {
		marginTop: '16px',
		marginBottom: '16px',
	},
}

const CurrencySelectionPage = ({
	primaryCurrencies,
	secondaryCurrencies,
	selectedCurrency,
	disabledCurrencies,
	classes,
	getTradeHistory,
	validPairsForSelectedCurrency
}) => (
	<div>
		{/* No time to implement search :(

		<Grid container className={ classes.gridContainer } justify="center" spacing={16}>
			{ selectedCurrency &&
				<Grid item xs={6} sm={6} md={4} lg={2} xl={1}>
					<CurrencyCardContainer currency={ selectedCurrency } to="/" active />
				</Grid>
			}
			<Grid item xs={12} sm={6} md={8} lg={10} xl={11}>
				<TextField
					className={ classes.searchField }
					id="searchCurrency"
					name="searchCurrency"
					label="Search currencies"
					InputProps={{ className: classes.searchFieldText }}
				/>
			</Grid>
		</Grid>*/}
		<CurrencyCardList
			heading="Primary currencies"
			currencies={ primaryCurrencies }
			selectedCurrency={ selectedCurrency }
			disabledCurrencies={ disabledCurrencies }
			validPairsForSelectedCurrency={ validPairsForSelectedCurrency }
		/>
		<CurrencyCardList
			heading="Secondary currencies"
			currencies={ secondaryCurrencies }
			selectedCurrency={ selectedCurrency }
			disabledCurrencies={ disabledCurrencies }
			validPairsForSelectedCurrency={ validPairsForSelectedCurrency }
		/>
	</div>
)

export default ProgressIfLoading (withStyles (styles) (CurrencySelectionPage))
