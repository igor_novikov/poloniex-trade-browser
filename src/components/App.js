import React from 'react'
import { Route, Redirect } from 'react-router'

import StateDisplay from '../containers/StateDisplay'
import AppHeader from './AppHeader'
import CurrencySelectionPageContainer from '../containers/CurrencySelectionPageContainer'
import TradeHistoryPageContainer from '../containers/TradeHistoryPageContainer'

import DeconstructParams from '../hocs/DeconstructParams'

const App = () => (
	<div>
		<StateDisplay disabled />
		<AppHeader />
		<Route
			exact path="/:leftCurrency([^_]+)_:rightCurrency([^_]+)/:page"
			component={ DeconstructParams (TradeHistoryPageContainer) }
		/>
		<Route
			exact path="/:selectedCurrency?"
			component={ DeconstructParams (CurrencySelectionPageContainer) }
		/>
	</div>
)

export default App
