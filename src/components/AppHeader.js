import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Link } from 'react-router-dom'

const AppHeader = () => (
	<AppBar position="static">
		<Toolbar>
			<Link to="/">
				<Typography variant="title" color="inherit">
					Currency Exchange
				</Typography>
			</Link>
		</Toolbar>
	</AppBar>
)

export default AppHeader
