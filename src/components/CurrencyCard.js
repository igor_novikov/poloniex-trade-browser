import React from 'react'

import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CardHeader from '@material-ui/core/CardHeader'
import grey from '@material-ui/core/colors/grey'
import blue from '@material-ui/core/colors/blue'
import { withStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import randomColor from 'randomcolor'

const styles = {
	button: {
		padding: '0',
		width: '100%',
		justifyContent: 'start',
	},
	inactive: {
		backgroundColor: grey [100],
	},
	active: {
		backgroundColor: blue [50],
	},
	avatar: {
		fontSize: '0.9em',
		fontWeight: 'bold',
		lineHeight: '1em',
		whiteSpace: 'pre-line',
		textAlign: 'center',
		textShadow: 'grey 0 0 1px',
	},
	currencyTitle: {
		textTransform: 'none',
	},
}

const CurrencyCard = ({ shortName, name, to, active, disabled, onClick, classes }) => (
	<Button
		className={ classes.button+' '+(active ? classes.active : classes.inactive) }
		variant="contained"
		component={ to && Link }
		to={ to }
		onClick={ onClick }
		disabled={ disabled }
	>
		<CardHeader
			avatar={
				<Avatar
					className={ classes.avatar }
					style={{ backgroundColor: randomColor ({ seed: shortName }) }}
				>
					{ shortName.length <= 4 ? shortName : shortName.match (/.{1,3}/g).join ('\n') }
				</Avatar>
			}
			title={
				<span className={ classes.currencyTitle }>
					{ name }
				</span>
			}
		/>
	</Button>
)

export default withStyles (styles) (CurrencyCard)
