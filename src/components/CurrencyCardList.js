import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import grey from '@material-ui/core/colors/grey'
import { withStyles } from '@material-ui/core/styles'

import CurrencyCardContainer from '../containers/CurrencyCardContainer'

const styles = {
	heading: {
		color: grey [800],
		marginTop: '24px',
		textAlign: 'center',
	},
	gridContainer: {
		marginTop: '16px',
		marginBottom: '16px',
	},
}

const CurrencyCardList = ({ currencies, selectedCurrency, disabledCurrencies, validPairsForSelectedCurrency, heading, classes }) => (
	<div>
		<Typography variant="display2" className={ classes.heading }>
			{ heading }
		</Typography>
		<Grid container justify="center" spacing={16} className={ classes.gridContainer }>
			{ currencies.map (currency => (
				<Grid key={ currency } item xs={6} sm={6} md={4} lg={2} xl={1}>
					<CurrencyCardContainer
						currency={ currency }
						active={ currency === selectedCurrency }
						to={
							currency === selectedCurrency ? '/' :
							selectedCurrency ? '/'+validPairsForSelectedCurrency [currency]+'/0' :
							'/'+currency
						}
						disabled={ disabledCurrencies [currency] }
					/>
				</Grid>
			)) }
		</Grid>
	</div>
)

export default withStyles (styles) (CurrencyCardList)
