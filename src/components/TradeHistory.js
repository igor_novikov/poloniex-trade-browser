import React from 'react'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TablePagination from '@material-ui/core/TablePagination'
import green from '@material-ui/core/colors/green'
import blue from '@material-ui/core/colors/blue'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router'
import DateTime from 'luxon/src/datetime'
import flow from 'lodash/flow'

import ProgressIfLoading from '../hocs/ProgressIfLoading'

const styles = {
	buyTransaction: {
		backgroundColor: green [50],
	},
	sellTransaction: {
		backgroundColor: blue [50],
	},
}

const Pagination = ({ page, count, history }) => (
	<TablePagination
		component="div"
		count={ count }
		rowsPerPage={ 20 }
		rowsPerPageOptions={ [] }
		page={ page }
		onChangePage={ (event, page) => history.push (String (page)) }
	/>
)

const TradeHistory = ({ transactions, page, history, classes }) => (
	<Paper elevation={4}>
		<Pagination page={ page } count={ transactions.length } history={ history } />
		<Table>
			<TableHead>
				<TableRow>
					<TableCell>Date</TableCell>
					<TableCell>Direction</TableCell>
					<TableCell numeric>Rate</TableCell>
					<TableCell numeric>Amount</TableCell>
					<TableCell numeric>Total</TableCell>
				</TableRow>
			</TableHead>
			<TableBody>
				{ transactions.slice (page * 20, page * 20 + 20).map (({ globalTradeID, date, type, rate, amount, total }) => {
					const convertedDate = DateTime
						.fromISO (date.replace (' ', 'T'), { zone: 'utc' })
						.toLocal()

					return (
						<TableRow key={ globalTradeID } className={ classes [type+'Transaction'] }>
							<TableCell>
								{ convertedDate.day === DateTime.local().day ? 'Today ' : 'Yesterday ' }
								{ convertedDate.toLocaleString (DateTime. TIME_24_WITH_SECONDS) }
							</TableCell>
							<TableCell>{ type }</TableCell>
							<TableCell numeric>{ rate }</TableCell>
							<TableCell numeric>{ amount }</TableCell>
							<TableCell numeric>{ total }</TableCell>
						</TableRow>
					)
				})}
			</TableBody>
		</Table>
		<Pagination page={ page } count={ transactions.length } history={ history } />
	</Paper>
)

export default flow (withStyles (styles), ProgressIfLoading, withRouter) (TradeHistory)
