import React from 'react'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

import ProgressIfLoading from '../hocs/ProgressIfLoading'
import CurrencyCardContainer from '../containers/CurrencyCardContainer'
import TradeHistoryContainer from '../containers/TradeHistoryContainer'

const styles = {
	gridContainer: {
		marginTop: '16px',
		marginBottom: '16px',
	},
}

const TradeHistoryPage = ({ currencies, leftCurrency, rightCurrency, page = 0, classes, getTradeHistory }) => (
	<div>
		<Grid container className={ classes.gridContainer } justify="center" spacing={16}>
			<Grid item xs={6} sm={6} md={4} lg={2} xl={1}>
				<CurrencyCardContainer currency={ leftCurrency } to={ rightCurrency ? '/'+rightCurrency : '/' } active />
			</Grid>
			<Grid item xs={6} sm={6} md={4} lg={2} xl={1}>
				<CurrencyCardContainer currency={ rightCurrency } to={ '/'+leftCurrency } active />
			</Grid>
		</Grid>
		<TradeHistoryContainer
			page={ Number (page) }
			onMissingContent={ () => getTradeHistory (leftCurrency, rightCurrency) }
			{...{ leftCurrency, rightCurrency }}
		/>
	</div>
)

export default ProgressIfLoading (withStyles (styles) (TradeHistoryPage))
