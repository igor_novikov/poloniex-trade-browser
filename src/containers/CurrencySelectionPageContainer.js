import { connect } from 'react-redux'
import mapValues from 'lodash/mapValues'
import sortBy from 'lodash/sortBy'
import pickBy from 'lodash/pickBy'

import CurrencySelectionPage from '../components/CurrencySelectionPage'

const mapStateToProps = ({ currencies, validPairs }, { selectedCurrency }) => {
	selectedCurrency = currencies && currencies [selectedCurrency] ? selectedCurrency : null
	const primaryCurrencies = currencies &&
		sortBy (Object.keys (pickBy (currencies, ({ isPrimary }) => isPrimary)), (key) => currencies [key].name)
	const secondaryCurrencies = currencies &&
		sortBy (Object.keys (pickBy (currencies, ({ isPrimary }) => !isPrimary)), (key) => currencies [key].name)
	const disabledCurrencies = !selectedCurrency ? {} :
		mapValues (
			pickBy (currencies, ({ shortName }) => (
				selectedCurrency != shortName &&
				!validPairs [shortName] [selectedCurrency] &&
				!validPairs [selectedCurrency] [shortName]
			)),
			() => true
		)
	const validPairsForSelectedCurrency = selectedCurrency && validPairs [selectedCurrency] || null
	return {
		isLoading: !currencies,
		primaryCurrencies, secondaryCurrencies, selectedCurrency, disabledCurrencies, validPairsForSelectedCurrency
	}
}

export default connect (mapStateToProps) (CurrencySelectionPage)
