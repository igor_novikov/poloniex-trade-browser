import { connect } from 'react-redux'

import TradeHistoryPage from '../components/TradeHistoryPage'
import { tradeCurrencyPair } from '../util'

const mapStateToProps = ({ currencies, tradeHistoryByPair }, { leftCurrency, rightCurrency }) => {
	leftCurrency = currencies && currencies [leftCurrency] ? leftCurrency : null
	rightCurrency = currencies && currencies [rightCurrency] ? rightCurrency : null

	const history = leftCurrency && rightCurrency && tradeHistoryByPair [tradeCurrencyPair (leftCurrency, rightCurrency)]
	if (history && history.isReverse) {
		const temp = leftCurrency
		leftCurrency = rightCurrency
		rightCurrency = temp
	}

	return {
		isLoading: !currencies,
		currencies: currencies && Object.keys (currencies),
		leftCurrency, rightCurrency,
	}
}

const mapDispatchToProps = {
	getTradeHistory: (leftCurrency, rightCurrency) => ({ type: 'GET_TRADE_HISTORY', leftCurrency, rightCurrency }),
}

export default connect (mapStateToProps, mapDispatchToProps) (TradeHistoryPage)
