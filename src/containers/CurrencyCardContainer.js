import { connect } from 'react-redux'

import CurrencyCard from '../components/CurrencyCard'

const mapStateToProps = ({ currencies }, { currency }) => {
	const { shortName, name } = currencies [currency]
	return { shortName, name }
}

export default connect (mapStateToProps, null, null, { pure: true }) (CurrencyCard)
