import { connect } from 'react-redux'

import TradeHistory from '../components/TradeHistory'
import { tradeCurrencyPair } from '../util'

const mapStateToProps = ({ tradeHistoryByPair }, { leftCurrency, rightCurrency }) => {
	const history = tradeHistoryByPair [tradeCurrencyPair (leftCurrency, rightCurrency)]
	const transactions = history && history.transactions || null
	return { transactions, isLoading: !history }
}

export default connect (mapStateToProps) (TradeHistory)
