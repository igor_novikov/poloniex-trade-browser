import React from 'react'
import { connect } from 'react-redux'

class StateDisplay extends React.Component {
	constructor() {
		super()
		this.state = { expand: false }
	}

	onToggle = () => {
		this.setState (({ expand }) => ({ expand: !expand }))
	}

	render() {
		const { state, disabled, onToggle } = this.props
		const { expand } = this.state
		if (disabled) return null
			
		return <div>
			<input
				style={{
					position: 'absolute',
					zIndex: '1000',
				}}
				type="checkbox"
				checked={ expand }
				onClick={ this.onToggle }
			/>
			{ expand &&
				<pre
					style={{
						position: 'absolute',
						color: 'white',
						textShadow: 'black -1px -1px 10px, black 1px -1px 10px, black -1px 1px 10px, black 1px 1px 10px',
						zIndex: '1000',
						pointerEvents: 'none',
					}}
				>
					{ JSON.stringify (state, undefined, '    ') }
				</pre>
			}
		</div>
	}
}

const stateToProps = (state) => ({ state })

export default StateDisplay = connect (stateToProps) (StateDisplay)
