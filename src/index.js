import 'babel-polyfill'

import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import { Router, Route } from 'react-router'
import { createBrowserHistory } from 'history'

import rootReducer from './reducers'
import rootSaga from './sagas'
import App from './components/App'

console.log ('app component', App)
import './index.scss';

const browserHistory = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()

export const store = window.store = createStore (
	rootReducer,
	applyMiddleware (sagaMiddleware)
)

sagaMiddleware.run (rootSaga)

rootSaga.ready.finally (() => {
	ReactDOM.render (
		<Provider store={ store }>
			<Router history={ browserHistory }>
				{d ('rendering', <App />)}
			</Router>
		</Provider>,
		document.getElementById ('root')
	)
})
